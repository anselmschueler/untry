//! This is a collection of macros that work the “opposite” way of the `?` operator.
//! That is, they early-return if the input is successful, and evaluate to the error (if there is any).
//!
//! This crate is `no_std`-compatible.
//! There is a nightly-exclusive item guarded behing the `nightly` feature.
#![no_std]
#![cfg_attr(any(feature = "nightly", docsrs), feature(try_trait_v2))]
#![cfg_attr(docsrs, feature(doc_cfg))]

/// The opposite of `?` for [`Option`].
///
/// This macro early-returns if the value is a [`Some`] variant,
/// and ignores any [`None`].
///
/// ```
/// # use untry::untry_option;
/// fn first_some<T, F1, F2>(f_1: F1, f_2: F2) -> Option<T>
/// where
///     F1: FnOnce() -> Option<T>,
///     F2: FnOnce() -> Option<T>
/// {
///     untry_option! { f_1() }
///     untry_option! { f_2() }
///     None
/// }
///
/// enum When {
///     One,
///     Two,    
/// }
///
/// assert!(
///     matches!(
///         first_some(|| Some(When::One), || Some(When::Two)),
///         Some(When::One)
///     )
/// );
///
/// assert!(
///     matches!(
///         first_some(|| Some(When::One), || None),
///         Some(When::One)
///     )
/// );
///
/// assert!(
///     matches!(
///         first_some(|| None, || Some(When::Two)),
///         Some(When::Two)
///     )
/// );
///
/// assert!(
///     matches!(
///         first_some::<When, _, _>(|| None, || None),
///         None
///     )
/// );
/// ```
#[macro_export]
macro_rules! untry_option {
    ($it: expr) => {
        if let it @ ::core::option::Option::Some(_) = $it {
            return it;
        }
    };
}

/// The opposite of `?` for [`Result`].
///
/// This macro early-returns if the value is an [`Ok`] variant,
/// otherwise it unwraps the error and evaulates to it.
///
/// [`Ok`] variants are unwrapped and rewrapped,
/// so the error type argument of the return [`Result`] type can differ
/// from the error type argument of the [`Result`] type of the value.
///
/// ```
/// # use untry::untry_result;
/// fn first_ok<T, E1, E2, E3, F1, F2, G>(f_1: F1, f_2: F2, join_err: G) -> Result<T, E3>
/// where
///     F1: FnOnce() -> Result<T, E1>,
///     F2: FnOnce() -> Result<T, E2>,
///     G: FnOnce(E1, E2) -> E3,
/// {
///     let err_1 = untry_result!(f_1());
///     let err_2 = untry_result!(f_2());
///     Err(join_err(err_1, err_2))
/// }
///
/// enum WhenOk {
///     One,
///     Two,    
/// }
///
/// struct ErrOne;
///
/// struct ErrTwo;
///
/// struct ErrThree;
///
/// fn join_err(when_1: ErrOne, when_2: ErrTwo) -> ErrThree {
///     ErrThree
/// }
///
/// assert!(
///     matches!(
///         first_ok::<_, ErrOne, ErrTwo, _, _, _, _>(
///             || Ok(WhenOk::One),
///             || Ok(WhenOk::Two),
///             join_err
///         ),
///         Ok(WhenOk::One)
///     )
/// );
///
/// assert!(
///     matches!(
///         first_ok::<_, ErrOne, _, _, _, _, _>(
///             || Ok(WhenOk::One),
///             || Err(ErrTwo),
///             join_err
///         ),
///         Ok(WhenOk::One)
///     )
/// );
///
/// assert!(
///     matches!(
///         first_ok::<_, _, ErrTwo, _, _, _, _>(
///             || Err(ErrOne),
///             || Ok(WhenOk::Two),
///             join_err
///         ),
///         Ok(WhenOk::Two)
///     )
/// );
///
/// assert!(
///     matches!(
///         first_ok::<WhenOk, _, _, _, _, _, _>(
///             || Err(ErrOne),
///             || Err(ErrTwo),
///             join_err
///         ),
///         Err(ErrThree)
///     )
/// );
/// ```
#[macro_export]
macro_rules! untry_result {
    ($it: expr) => {
        match $it {
            ::core::result::Result::Ok(it) => return ::core::result::Result::Ok(it),
            ::core::result::Result::Err(err) => err,
        }
    };
}

/// Like [`untry_result`], but it swaps [`Ok`] for [`Some`].
///
/// It early-returns if the value is an [`Ok`] variant, rewrapping in [`Some`],
/// otherwise it unwraps the error and evaluates to it.
///
/// ```
/// # use untry::untry_ok_to_some;
/// fn maybe_recover<T, E, F, G>(callback: F, recover: G) -> Option<T>
/// where
///     F: FnOnce() -> Result<T, E>,
///     G: FnOnce(E) -> Option<T>,
/// {
///     recover(untry_ok_to_some!(callback()))
/// }
///
/// enum Where {
///     Callback,
///     Recover,
/// }
///
/// assert!(
///     matches!(
///         maybe_recover(|| Ok(Where::Callback), |()| Some(Where::Recover)),
///         Some(Where::Callback)
///     )
/// );
///
/// assert!(
///     matches!(
///         maybe_recover(|| Ok(Where::Callback), |()| None),
///         Some(Where::Callback)
///     )
/// );
///
/// assert!(
///     matches!(
///         maybe_recover(|| Err(()), |()| Some(Where::Recover)),
///         Some(Where::Recover)
///     )
/// );
///
/// assert!(
///     matches!(
///         maybe_recover(|| Err(()), |()| None::<Where>),
///         None
///     )
/// );
/// ```
#[macro_export]
macro_rules! untry_ok_to_some {
    ($it: expr) => {
        match $it {
            ::core::result::Result::Ok(it) => return ::core::option::Option::Some(it),
            ::core::result::Result::Err(err) => err,
        }
    };
}

/// Almost the opposite of `?` for arbitrary [`Try`] types.
///
/// Requires nightly Rust.
///
/// If [`Try::branch`] returns a [`ControlFlow::Continue`] variant, this macro returns immediately,
/// calling [`Try::from_output`] to reconstruct the [`Try`] value.
/// Otherwise, it evaluates to the residual in the [`ControlFlow::Break`] variant.
///
/// Because the value is reconstructed after the [`Try::branch`] call,
/// the return [`Try`] type doesn’t need to be the same one as the input [`Try`] type.
///
/// In the future, to get at the error when using this with [`Result`],
/// you will probably be able to use [`Result::into_err`].
/// At the moment you must use [`Result::unwrap_err`].
///
/// ```
/// #![feature(try_trait_v2)]
/// # use untry::untry_residual;
/// fn first_some<T, F1, F2>(f_1: F1, f_2: F2) -> Option<T>
/// where
///     F1: FnOnce() -> Option<T>,
///     F2: FnOnce() -> Option<T>
/// {
///     untry_residual!(f_1());
///     untry_residual!(f_2());
///     None
/// }
///
/// enum When {
///     One,
///     Two,    
/// }
///
/// assert!(
///     matches!(
///         first_some(|| Some(When::One), || Some(When::Two)),
///         Some(When::One)
///     )
/// );
///
/// assert!(
///     matches!(
///         first_some(|| Some(When::One), || None),
///         Some(When::One)
///     )
/// );
///
/// assert!(
///     matches!(
///         first_some(|| None, || Some(When::Two)),
///         Some(When::Two)
///     )
/// );
///
/// assert!(
///     matches!(
///         first_some::<When, _, _>(|| None, || None),
///         None
///     )
/// );
/// ```
///
/// ```
/// #![feature(try_trait_v2)]
/// # use untry::untry_residual;
/// fn first_ok<T, E1, E2, E3, F1, F2, G>(f_1: F1, f_2: F2, join_err: G) -> Result<T, E3>
/// where
///     F1: FnOnce() -> Result<T, E1>,
///     F2: FnOnce() -> Result<T, E2>,
///     G: FnOnce(E1, E2) -> E3,
/// {
///     let err_1 = untry_residual!(f_1()).unwrap_err();
///     let err_2 = untry_residual!(f_2()).unwrap_err();
///     Err(join_err(err_1, err_2))
/// }
///
/// enum WhenOk {
///     One,
///     Two,    
/// }
///
/// struct ErrOne;
///
/// struct ErrTwo;
///
/// struct ErrThree;
///
/// fn join_err(when_1: ErrOne, when_2: ErrTwo) -> ErrThree {
///     ErrThree
/// }
///
/// assert!(
///     matches!(
///         first_ok::<_, ErrOne, ErrTwo, _, _, _, _>(
///             || Ok(WhenOk::One),
///             || Ok(WhenOk::Two),
///             join_err
///         ),
///         Ok(WhenOk::One)
///     )
/// );
///
/// assert!(
///     matches!(
///         first_ok::<_, ErrOne, _, _, _, _, _>(
///             || Ok(WhenOk::One),
///             || Err(ErrTwo),
///             join_err
///         ),
///         Ok(WhenOk::One)
///     )
/// );
///
/// assert!(
///     matches!(
///         first_ok::<_, _, ErrTwo, _, _, _, _>(
///             || Err(ErrOne),
///             || Ok(WhenOk::Two),
///             join_err
///         ),
///         Ok(WhenOk::Two)
///     )
/// );
///
/// assert!(
///     matches!(
///         first_ok::<WhenOk, _, _, _, _, _, _>(
///             || Err(ErrOne),
///             || Err(ErrTwo),
///             join_err
///         ),
///         Err(ErrThree)
///     )
/// );
/// ```
///
/// ```
/// #![feature(try_trait_v2)]
/// # use untry::untry_residual;
/// fn maybe_recover<T, E, F, G>(callback: F, recover: G) -> Option<T>
/// where
///     F: FnOnce() -> Result<T, E>,
///     G: FnOnce(E) -> Option<T>,
/// {
///     recover(untry_residual!(callback()).unwrap_err())
/// }
///
/// enum Where {
///     Callback,
///     Recover,
/// }
///
/// assert!(
///     matches!(
///         maybe_recover(|| Ok(Where::Callback), |()| Some(Where::Recover)),
///         Some(Where::Callback)
///     )
/// );
///
/// assert!(
///     matches!(
///         maybe_recover(|| Ok(Where::Callback), |()| None),
///         Some(Where::Callback)
///     )
/// );
///
/// assert!(
///     matches!(
///         maybe_recover(|| Err(()), |()| Some(Where::Recover)),
///         Some(Where::Recover)
///     )
/// );
///
/// assert!(
///     matches!(
///         maybe_recover(|| Err(()), |()| None::<Where>),
///         None
///     )
/// );
/// ```
///
/// [`Try`]: core::ops::Try
/// [`Try::branch`]: core::ops::Try::branch
/// [`ControlFlow::continue`]: core::ops::ControlFlow::Continue
/// [`Try::from_output`]: core::ops::Try::from_output
/// [`ControlFlow::break`]: core::ops::ControlFlow::Break
#[cfg_attr(docsrs, doc(cfg(feature = "nightly")))]
#[cfg(any(feature = "nightly", docsrs))]
#[macro_export]
macro_rules! untry_residual {
    ($it: expr) => {
        match ::core::ops::Try::branch($it) {
            ::core::ops::ControlFlow::Continue(it) => return ::core::ops::Try::from_output(it),
            ::core::ops::ControlFlow::Break(residual) => residual,
        }
    };
}
