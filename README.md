# Untry

Untry provides macros that do the “opposite” of the `?` operator, in a way.

See the [documentation](https://docs.rs/untry/0.1.0/untry) for details.
